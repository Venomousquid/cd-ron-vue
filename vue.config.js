module.exports = {
	transpileDependencies: true,
	publicPath: './',
	productionSourceMap: false,
	css: {
		loaderOptions: {
			scss: {
				additionalData: `
				@import "@/scss/colors.scss";
				@import "@/scss/breakpoints.scss";`
			}
		}
	}
}
