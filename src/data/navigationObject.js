import beginOpnieuw from "@/assets/begin_opnieuw.webp";
import blauwSchermFilm from "@/assets/blauw_scherm_film.webp";
import blauwScherm from "@/assets/blauw_scherm.webp";
import madJam from "@/assets/mad_jam.webp";
import nachtvlinders from "@/assets/nachtvlinders.webp";
import podcast from "@/assets/podcast.webp";
import regenboog from "@/assets/regenboog.webp";
import schoonmoeder from "@/assets/schoonmoeder.webp";
import tabs from "@/assets/tabs.webp";
import whatsapp from "@/assets/whatsapp.webp";

export const navigationObject = [
    {
        name: 'Theater Projecten',
        content: [
            { 
                title: '2022 - Blauw Scherm (toneelmeester)',
                img: blauwScherm,
                text: 'Irene & Co - Theater Zuidplein'
            },
            { 
                title: '2016 - Nachtvlinders (geluidstechniek)',
                img: nachtvlinders,
                text: 'Werklicht - Huis van de Wijk \'t Klooster'
            },
            { 
                title: '2014 - Gooi je Schoonmoeder uit het Raam (licht- en geluidstechniek)',
                img: schoonmoeder,
                text: 'Werklicht - Huis van de Wijk Irene / Millinxparkhuis'
            },
            { 
                title: ' 2013 - Ergens Achter de Regenboog (licht- en geluidstechniek)',
                img: regenboog,
                text: 'Werklicht - Huis van de Wijk Irene / Millinxparkhuis'
            },
        ]
    },
    {
        name: 'Muziek Projecten',
        content: [
            { 
                title: ' 2023/heden - CD-RON (producer / vocalist)',
                img: '',
                text: ''
            },
            { 
                title: ' 2014/heden - TABS (bassist / geluidstechniek)',
                img: tabs,
                text: 'TABS'
            },
            { 
                title: '2019/heden - Mad Jam (bassist)',
                img: madJam,
                text: 'Mad Jam'
            },
        ]
    },
    {
        name: 'Film Projecten',
        content: [
            { 
                title: '2022/2023 - Blauw Scherm (licht-, geluidstechniek en geluid postproductie)',
                img: blauwSchermFilm,
                text: 'Blauw Scherm'
            },
        ]
    },
    {
        name: 'Podcasts',
        content: [
        { 
                title: '2022 - Theatergroep Hoogvliet (opnemen en afmixen)',
                img: podcast,
                text: 'Podcast Theatergroep Hoogvliet'
            },
            { 
                title: ' 2023 - Theatergroep Irene & Co (opnemen en afmixen)',
                img: beginOpnieuw,
                text: 'Podcast Theatergroep Irene & Co'
            },
        ]
    },
    {
        name: 'WhatsApp',
        content: [
        { 
                title: 'WhatsApp',
                img: whatsapp,
                text: 'Klik om WhatsApp te openen'
            },
        ]
    },
]
